import React from "react";
import { Navigate } from "react-router-dom";
import { getItem } from "./helper/localStorage";

export const ProtectRoute = ({ children, normal = false, role }) => {
    try {
        let lData = getItem("miq-user-info");
        let token = getItem("miq-user-token");
        if (token && lData && Object.keys(lData).includes("id")) {
            
            if (normal) { // for demo normal is false alue pass other wise pass condition base
                return <Navigate to="/dashboard" replace />;
            } else {
                return <>{children}</>;

                // if (role.includes(lData.role)) {
                //     return <>{children}</>;
                // } else {
                //     return <Navigate to="/dashboard" />;
                // }
            }
        }
        // return <Navigate to="/" />;     for testing comment this condition
        return <Navigate to="/dashboard" />;
    } catch (error) {
        return <Navigate to="/" />;
    }
};
