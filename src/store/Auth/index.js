/* eslint-disable */
import { message } from "antd";
import { makeAutoObservable, runInAction } from "mobx";
import AuthService from "../../services/Auth.service";
import { getItem, setItem } from "../../helper/localStorage";

class AuthStore {
    isFetchingData = false;
    showChangePassword = false;
    showUserProfile = false;

    constructor(allStores) {
        makeAutoObservable(this);
        this.allStores = allStores;
    }

    reset = () => {
        this.isFetchingData = false;
        this.showChangePassword = false;
        this.showUserProfile = false;
    };

    clearStore = async () => {
        this.reset();
        this.allStores.dashboard.reset();
        // this.allStores.customer.reset();
        // this.allStores.user.reset();
    };

    login = async (values) => {
        try {
            this.isFetchingData = true;
            message.success("Successfully logged in =====> Testing");
            this.isFetchingData = false;
            this.allStores.routes.history("/dashboard");

            // const data = await AuthService.loginService(values);
            // if (data.status === 200) {
            //     if (data.data.result) {
            //         if (data.data.result.user) {
            //             localStorage.clear();
            //             sessionStorage.clear();
            //         }
            //         message.success("Successfully logged in");
            //         this.isFetchingData = false;
            //         this.allStores.routes.history("/dashboard");
            //     } else {
            //         message.error("Authentication Failed.!");
            //         this.isFetchingData = false;
            //     }
            // }
        } catch (error) {
            message.error(error.response.data.message || error || "Something went Wrong.!");
            this.isFetchingData = false;
        }
    };

    resetPassword = async (values) => {
        try {
            this.isFetchingData = true;
            const data = await AuthService.resetPasswordService(values);

            if (data.status === 200) {
                message.success(data.data.message);
                this.isFetchingData = false;
                this.logout();
            }
        } catch (error) {
            message.error(error.response.data.message || error || "Something went Wrong.!");
            this.isFetchingData = false;
        }
    };

    verifyEmail = async (token) => {
        try {
            this.isFetchingData = true;
            const data = await AuthService.verifyForgotEmailService(token);
            if (data.status < 400) {
                message.success(data.data.message);
                this.isFetchingData = false;
                this.allStores.routes.history("/");
            }
        } catch (error) {
            message.error(error.response.data.message || error || "Something went Wrong.!");
            this.isFetchingData = false;
        }
    };

    setPassword = async (values, token) => {
        try {
            this.isFetchingData = true;
            const data = await AuthService.setPasswordService(values, token);
            if (data.status < 400) {
                message.success("Reset password successfully");
                this.isFetchingData = false;
                this.allStores.routes.history("/");
            }
        } catch (error) {
            message.error(error.response.data.message || error || "Something went Wrong.!");
            this.isFetchingData = false;
        }
    };

    forgotPassword = async (values) => {
        try {
            this.isFetchingData = true;
            const data = await AuthService.forgotPasswordService(values);
            if (data.status < 400) {
                message.success("Reset password link has been sent to registered email");
                this.isFetchingData = false;

                this.allStores.routes.history("/");
            }
        } catch (error) {
            message.error(error.response.data.message || error || "Something went Wrong.!");
            this.isFetchingData = false;
        }
    };

    logout = async () => {
        try {
            const refresh_token = getItem("miq-user-rf-token");
            const data = await AuthService.logoutService({ refreshToken: refresh_token });
            if (data.status <= 204) {
                await this.clearStore();
                localStorage.clear();
                this.allStores.routes.history("/dashboard");
            }
        } catch (error) {
            message.error(error?.response?.data?.message || error || "Something went Wrong.!");
        }
    };

    openChangePassword = async (visibility) => {
        runInAction(() => {
            this.showChangePassword = visibility;
        });
    };

    openUserProfile = async (visibility) => {
        runInAction(() => {
            this.showUserProfile = visibility;
        });
    };
}

export default AuthStore;
