import AuthStore from "./Auth";
import DashboardStore from "./Dashboard";
import ManageUserStore from "./UserMaster";
import RouterStore from "./routeStore";
import PopupModalStore from "./Modal";

class CombinedStore {
    constructor(allStore) {
        this.routes = new RouterStore(this);
        this.resetAllStore();
    }

    resetAllStore = () => {
        this.auth = new AuthStore(this);
        this.user = new ManageUserStore(this);
        this.dashboard = new DashboardStore(this);
        this.popups = new PopupModalStore(this);
    };
}
export default CombinedStore;
