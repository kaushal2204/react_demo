/* eslint-disable */
import { makeAutoObservable, runInAction } from "mobx";

class PopupModalStore {
    subscriptionModalVisibility = false;
    subscriptionIsEdit = false;

    constructor(allStores) {
        makeAutoObservable(this);
        this.allStores = allStores;
    }

    reset = () => {
        this.subscriptionModalVisibility = false;
    };

    changeSubscriptionModalVisibility = async (visibility, edit = false) => {
        runInAction(() => {
            this.subscriptionModalVisibility = visibility;
            this.subscriptionIsEdit = edit;
        });
    };
}

export default PopupModalStore;
