/* eslint-disable */
import { message } from "antd";
import { makeAutoObservable } from "mobx";
import DashboardService from "../../services/Dashboard.service";

class DashboardStore {
    isFetchingData = false;
    dashboardData = [];

    constructor(allStores) {
        makeAutoObservable(this);
        this.allStores = allStores;
    }

    reset = () => {
        this.isFetchingData = false;
        this.dashboardData = [];
    };

    getDashboard = async () => {
        try {
            this.isFetchingData = true;
            const data = await DashboardService.fetchDashboardService();

            if (data.status < 400) {
                this.dashboardData = data.data.result;
                this.isFetchingData = false;
            }
        } catch (error) {
            message.error(error.response.data.message || error || "Something went Wrong.!");
            this.isFetchingData = false;
        }
    };
}

export default DashboardStore;
