import { inject, observer } from "mobx-react";
import React, { useEffect } from "react";
import { Route, Routes, useLocation, useNavigate, useParams } from "react-router-dom";
import NormalRoute from "./NormalRoute";
import MainComp from "./components";
import ForgotPassword from "./components/pages/auth/forgotPassword";
import ResetPassword from "./components/pages/auth/resetPassword";
import Login from "./components/pages/auth/index";
import Dashboard from "./components/pages/dashboard";
import Page404 from "./components/pages/404";
import { ProtectRoute } from "./Protected";

function App({ routes }) {
    let location = useLocation();
    let navigate = useNavigate();
    let params = useParams();

    useEffect(() => {
        routes.setRoute(location, params, navigate);
    });

    return (
        <Routes>
            <Route path="*" element={<Page404 />} />
            <Route
                path="/"
                element={
                    <NormalRoute>
                        <Login />
                    </NormalRoute>
                }
            />
            <Route
                path="/forgot-password"
                element={
                    <NormalRoute>
                        <ForgotPassword />
                    </NormalRoute>
                }
            />
            <Route
                path="/reset-password"
                element={
                    <NormalRoute>
                        <ResetPassword />
                    </NormalRoute>
                }
            />
            <Route
                path="/dashboard"
                element={
                    <ProtectRoute role={["admin"]}>
                        <MainComp comp={<Dashboard />} />
                    </ProtectRoute>
                }
            />
        </Routes>
    );
}

export default inject("routes")(observer(App));
