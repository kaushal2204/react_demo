import { Provider } from "mobx-react";
import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./Routes";
import "./assets/less/index.less";
import reportWebVitals from "./reportWebVitals";
import CombinedStore from "./store/CombinedStore";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <Provider {...new CombinedStore()}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

reportWebVitals();
