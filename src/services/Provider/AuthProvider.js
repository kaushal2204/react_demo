import axios from "axios";
import { getItem, setItem } from "../../helper/localStorage";

// Add a request interceptor
axios.interceptors.request.use(
    (config) => {
        const token = getItem("miq-user-token");

        if (token) {
            config.headers["Authorization"] = "Bearer " + token;
            config.headers["Content-Type"] = "application/json";
        }
        config.headers["Content-Type"] = "application/json";
        return config;
    },
    (error) => {
        Promise.reject(error);
    }
);

axios.interceptors.response.use(
    (response) => {
        return response;
    },
    async function (error) {
        const originalRequest = error.config;

        if (error.response.status === 401 && originalRequest.url === `${process.env.REACT_APP_API_ENDPOINT}/v1/auth/refresh-tokens`) {
            localStorage.clear();
            window.location.href = "/";
            return Promise.reject(error);
        }

        if (error.response.status === 401 && originalRequest.url === `${process.env.REACT_APP_API_ENDPOINT}/v1/auth/login`) {
            return Promise.reject(error);
        }

        if (error.response.status === 401 && originalRequest.url.includes("verify-email")) {
            return Promise.reject(error);
        }

        if (error.response.status === 401 && originalRequest.url.includes("reset-password-byemail")) {
            return Promise.reject(error);
        }

        if (error.response.status === 401 && originalRequest.url.includes("reset-password")) {
            return Promise.reject(error);
        }

        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;
            const refreshToken = getItem("miq-user-rf-token");
            return axios
                .post(`${process.env.REACT_APP_API_ENDPOINT}/v1/auth/refresh-tokens`, {
                    refreshToken: refreshToken,
                })
                .then((res) => {
                    if (res.status === 200) {
                        setItem("miq-user-token", res.data.access_token);
                        setItem("miq-user-rf-token", res.data.refresh_token);
                        axios.defaults.headers.common["Authorization"] = "Bearer " + res.data.access_token;
                        return axios(originalRequest);
                    }
                });
        }
        return Promise.reject(error);
    }
);

export default axios;
