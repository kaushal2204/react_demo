// import axios from "axios";
import axios from "./Provider/AuthProvider";

class DashboardService {
    fetchDashboardService = async () => {
        let url = `${process.env.REACT_APP_API_ENDPOINT}/v1/dashboard`;
        return new Promise((resolve, reject) => {
            axios
                .get(url)
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
}
export default new DashboardService();
