import React from "react";
import { Layout } from "antd";
const { Footer } = Layout;

export default function MyFooter() {
    return (
        <Footer
            style={{
                textAlign: "center",
                color: "#5144A0",
            }}
        >
            ©{new Date().getFullYear()} Created by Team kaushal patel
        </Footer>
    );
}
