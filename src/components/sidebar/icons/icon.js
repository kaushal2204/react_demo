import Icon from "@ant-design/icons";
import { SvgDashboard, SvgCustomers, SvgProjects, SvgManageUsers } from "./iconSvg";

export const IconDashboard = (props) => <Icon component={SvgDashboard} {...props} />;
export const IconCustomers = (props) => <Icon component={SvgCustomers} {...props} />;
export const IconProjects = (props) => <Icon component={SvgProjects} {...props} />;
export const IconSetting = (props) => <Icon component={SvgManageUsers} {...props} />;
