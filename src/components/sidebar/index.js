import { Layout, Menu } from "antd";
// import React, { useState } from "react";
import React from "react";
import { NavLink, useLocation } from "react-router-dom";
import logo from "../../assets/logo/logo.png";
import logo2 from "../../assets/logo/logo2.png";
import { IconCustomers, IconDashboard, IconProjects, IconSetting } from "./icons/icon";
const { Sider } = Layout;

export default function MySideBar(props) {
    const items = [
        {
            key: "Dashboard",
            icon: <IconDashboard />,
            label: <NavLink to="/dashboard">Dashboard</NavLink>,
            role: ["admin", "sm_user", "customer", "customer_user"],
        },
        // {
        //     key: "UserMaster",
        //     icon: <IconCustomers />,
        //     label: <NavLink to="/user-master">User Master</NavLink>,
        //     role: ["admin"],
        // },
    ];

    const { pathname } = useLocation();
    // const [brkPoint, setBrkPoint] = useState(false);
    const allItems = items.filter((elm) => elm.role.includes("admin"));
    let selectedKey = allItems.find((o) => o.to === pathname);

    if (!selectedKey) {
        if (pathname === "/dashboard") {
            selectedKey = "Dashboard";
        } else if (pathname.includes("/user-master")) {
            selectedKey = "UserMaster";
        } else if (pathname.includes("/subscriptions")) {
            selectedKey = "Subscriptions";
        } else if (pathname.includes("/enquiries")) {
            selectedKey = "Enquiries";
        } else if (pathname.includes("/reports")) {
            selectedKey = "Reports";
        } else if (pathname.includes("/cms-and-faqs")) {
            selectedKey = "CmsFaqs";
        } else {
            selectedKey = "Dashboard";
        }
    } else {
        selectedKey = selectedKey.key;
    }

    return (
        <>
            <Sider
                collapsed={props.collapsed}
                // onBreakpoint={(data) => {
                //     setBrkPoint(data);
                // }}
                className="rgb-sidebar"
                breakpoint="lg"
                // collapsedWidth={brkPoint ? 0 : 80}
                theme="light"
                width={250}
                style={{
                    boxShadow: "1px 4px 4px rgba(255,255,255, 0.16)",
                }}
            >
                <div
                    className="logo"
                    style={{
                        width: "100%",
                        height: "5.5em",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        padding: "0.5em",
                    }}
                >
                    <img alt="PM" src={props.collapsed ? logo2 : logo} style={{ height: "100%" }} />
                </div>

                <Menu className={props.collapsed ? "mt-2 colsp bg-sidebar" : "mt-2 bg-sidebar"} theme="light" items={items.filter((elm) => elm.role.includes("admin"))} mode="inline" selectedKeys={selectedKey}></Menu>
            </Sider>
        </>
    );
}
