import React from "react";
import MainLogo from "../../assets/logo/logo.png";

export const Logo = (props) => {
    return <img src={MainLogo} {...props} alt="Steel Mains" />;
};
