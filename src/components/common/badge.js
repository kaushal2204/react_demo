import React from "react";

export const Badge = ({ text, status = "active" }) => {
    return (
        <>
            <div className={status.toLowerCase() === "active" ? "mq-badge mq-badge-active" : "mq-badge mq-badge-inactive"}>{text}</div>
        </>
    );
};
