import { Table } from "antd";
import React from "react";

export const CustomTable = (props) => {
    return (
        <div className="custom-table">
            {props.title && props.children ? (
                <div className="custom-table-titles">
                    <p>{props.title}</p>
                    {props.children}
                </div>
            ) : (
                <></>
            )}
            <Table
                rowClassName={props.rowClassName}
                style={props.style}
                components={props.components}
                rowKey={props?.rowKey}
                scroll={props.scroll}
                bordered={props.border}
                columns={props.columns}
                dataSource={props.data}
                loading={props.loading}
                rowSelection={props.rowSelection}
                pagination={props.pagination}
                onChange={props.onChange}
            />
        </div>
    );
};
