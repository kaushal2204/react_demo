import { LockOutlined, MenuFoldOutlined, MenuUnfoldOutlined, PoweroffOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Dropdown, Form, Layout } from "antd";
import { inject, observer } from "mobx-react";
import React from "react";
import userProfile from "../../assets/images/userProfile.png";
import CustomPassword from "../common/indexPassword";
import { CustomModal } from "../common/modal";
const { Header } = Layout;

function MyHeader(props) {
    const [changePasswordForm] = Form.useForm();

    const items = [
        {
            key: "1",
            icon: <LockOutlined />,
            label: (
                <div onClick={() => props.auth.openChangePassword(true)} className="header-action-item">
                    Change Password
                </div>
            ),
        },
        {
            key: "2",
            icon: <UserOutlined />,
            label: (
                <div onClick={() => props.auth.openUserProfile(true)} className="header-action-item">
                    Profile
                </div>
            ),
        },
        {
            key: "3",
            icon: <PoweroffOutlined />,
            danger: true,
            label: (
                <div onClick={() => props.auth.logout()} className="header-action-item">
                    Logout
                </div>
            ),
        },
    ];

    const handleFinish = async (values) => {
        delete values.confirm_password;
        await props.auth.resetPassword(values);
    };

    return (
        <>
            <Header
                theme="light"
                className={`site-layout-sub-header-background ${props.collapsed ? "w_80" : "w_250"}`}
                style={{
                    padding: 0,
                    background: "white",
                }}
            >
                {props.collapsed ? (
                    <MenuUnfoldOutlined
                        className="colaps-icons header-icon"
                        style={{
                            marginLeft: "0.85em",
                            padding: "0.4em 0.8em",
                            fontSize: "1.2rem",
                            border: "2px #ffffff45 solid",
                            borderRadius: "0.2em",
                        }}
                        onClick={() => props.setCollapsed(!props.collapsed)}
                    />
                ) : (
                    <MenuFoldOutlined
                        className="colaps-icons header-icon"
                        style={{
                            marginLeft: "0.85em",
                            padding: "0.4em 0.8em",
                            fontSize: "1.2rem",
                            border: "2px #ffffff45 solid",
                            borderRadius: "0.2em",
                        }}
                        onClick={() => props.setCollapsed(!props.collapsed)}
                    />
                )}
                <div className="header-actions">
                    <Dropdown
                        className="px-3"
                        trigger="click"
                        placement="bottomRight"
                        // arrow
                        menu={{ items }}
                    >
                        <div className="avatar d-flex cursor-pointer">
                            <img alt="MIQ" src={userProfile} />
                        </div>

                        {/* <p className="fs-11 cursor-pointer header-profile">
                            Metrics iQ <DownOutlined />
                        </p> */}
                    </Dropdown>
                </div>
            </Header>

            <CustomModal visibility={props.auth.showChangePassword} handleCancel={props.auth.openChangePassword} className="add-customer-modal" centered title="Change Password">
                <div className="add-customer-modal">
                    <Form layout="vertical" id="changePasswordForm" form={changePasswordForm} onFinish={handleFinish}>
                        <Form.Item
                            label="Old Password"
                            name="old_password"
                            rules={[
                                {
                                    required: true,
                                    message: "Please enter Old Password",
                                },
                            ]}
                        >
                            <CustomPassword className="w-100 p-1H7 fs-1" />
                        </Form.Item>
                        <Form.Item
                            label="New Password"
                            name="new_password"
                            rules={[
                                {
                                    required: true,
                                    message: "Please enter New Password",
                                },
                                {
                                    min: 8,
                                    message: "Password needs to be at least 8 characters long.",
                                },
                            ]}
                        >
                            <CustomPassword className="w-100 p-1H7 fs-1" />
                        </Form.Item>
                        <Form.Item
                            label="Confirm Password"
                            name="confirm_password"
                            rules={[
                                {
                                    required: true,
                                    message: "Please enter Confirm Password",
                                },
                                ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                        if (!value || getFieldValue("new_password") === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject("New Password & Confirm Password are not same");
                                    },
                                }),
                            ]}
                        >
                            <CustomPassword className="w-100 p-1H7 fs-1" />
                        </Form.Item>
                        <Button block htmlType="submit" size="large" loading={props.auth.isFetchingData} className="mt-2 bg-primary text-white btn-custome">
                            Change
                        </Button>
                    </Form>
                </div>
            </CustomModal>

            <CustomModal visibility={props.auth.showUserProfile} handleCancel={props.auth.openUserProfile} className="add-customer-modal" centered title="Profile">
                <div class="user-picture">
                    <svg viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg">
                        <path d="M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z"></path>
                    </svg>
                </div>
                <p class="profile-name-client">
                    {" "}
                    Admin
                    <span>admin@gmail.com</span>
                </p>
                {/* <div className="add-customer-modal">
                    <div className="user-prfile-box">
                        <div className="user-avtar">
                            <UserOutlined />
                        </div>
                        <div className="user-text">
                            <label>Name</label>
                            Metrics iQ
                        </div>
                        <div className="user-text">
                            <label>Username ( Login )</label>
                            Metrics iQ
                        </div>
                        <div className="user-text">
                            <label>Email</label>
                            Metrics iQ
                        </div>
                    </div>
                </div> */}
            </CustomModal>
        </>
    );
}

export default inject("auth")(observer(MyHeader));
