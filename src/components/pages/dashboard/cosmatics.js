import React from "react";

export const Cosmatics = (props) => {
    return (
        <div className="dashboard-cosmatic">
            {/* <div className="top-right-icon">{props.icon}</div>
            <div className="cosmatic-icon">{props.icon}</div> */}
            <div className="cosmatic-counts"> {props.counts}</div>
            <p className="cosmatic-title">{props.title}</p>
        </div>
    );
};
