import { Col, Row } from "antd";
import { inject, observer } from "mobx-react";
import React, { useEffect } from "react";
import { Cosmatics } from "./cosmatics";
import { CustomTable } from "../../common/tables";

const Dashboard = ({ dashboard }) => {
    const columns = [
        {
            title: "No.",
            dataIndex: "key",
            render: (_, record, index) => {
                return index + 1;
            },
        },
        {
            title: "Name",
            dataIndex: "name",
            sorter: (a, b) => a.name - b.name,
        },
        {
            title: "Business Email",
            dataIndex: "email",
            sorter: (a, b) => a.email - b.email,
        },
        {
            title: "Contact Number",
            dataIndex: "mobile",
            sorter: (a, b) => a.mobile - b.mobile,
        },
        {
            title: "Registered On",
            dataIndex: "createdAt",
        },
        {
            title: "Expire On",
            dataIndex: "Expire",
        },
    ];

    // const onChange = (pagination, _, sort) => {
    //     if (sort || Object.keys(sort) || Object.keys(sort).length > 0) {
    //         user.getAllUsers({ page: pagination.current, pageSize: pagination.pageSize, sort, active: true });
    //         return;
    //     }
    // };
    // useEffect(() => {
    //     user.getAllUsers({ page: 1, active: true });
    // }, []);

    return (
        <>
            <Row gutter={24}>
                <Col className="mt-2" xs={24} sm={24} md={12} lg={6} xl={6} xxl={6}>
                    <Cosmatics title="Total Users" counts={359} />
                </Col>
                <Col className="mt-2" xs={24} sm={24} md={12} lg={6} xl={6} xxl={6}>
                    <Cosmatics title="Total Products Added" counts={1098} />
                </Col>
                <Col className="mt-2" xs={24} sm={24} md={12} lg={6} xl={6} xxl={6}>
                    <Cosmatics title="Total Competitors" counts={"20,154"} />
                </Col>
                <Col className="mt-2" xs={24} sm={24} md={12} lg={6} xl={6} xxl={6}>
                    <Cosmatics title="Total Revenues" counts={"$ 5550"} />
                </Col>
            </Row>
            {/* <Row className="mt-2" gutter={24}>
                <Col className="mt-2" xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                    <CustomTable
                        rowKey="key"
                        rowClassName="table-second"
                        style={{ minHeight: 500 }}
                        loading={user.isFetchingData}
                        data={user.userData}
                        columns={columns}
                        pagination={{ total: user.totalUser, current: user.page }}
                        onChange={onChange}
                        scroll={{ x: "100%" }}
                    />
                </Col>
            </Row> */}
        </>
    );
};

export default inject("dashboard")(observer(Dashboard));
