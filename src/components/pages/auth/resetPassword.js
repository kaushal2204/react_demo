import { Button, Col, Form, Input, Row } from "antd";
import { inject, observer } from "mobx-react";
import forgot_password from "../../../assets/images/forgot_password.svg";
import "../../../assets/less/index.less";
import { useSearchParams } from "react-router-dom";
import { useEffect } from "react";

const ResetPassword = ({ auth }) => {
    const [searchParams] = useSearchParams();
    const token = searchParams.get("token");

    useEffect(() => {
        if (token) {
            auth.verifyEmail(token);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleFinish = (values) => {
        auth.setPassword(values, token);
    };

    return (
        <>
            <div className="miq-theme content forgot-password">
                <div className="box">
                    <Row gutter={24} className="img">
                        <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
                            <div className="img">
                                <img alt="Img" src={forgot_password} />
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
                            <div className="form">
                                <h1>Set New Password</h1>
                                <Form onFinish={handleFinish} layout="vertical">
                                    <Form.Item
                                        label="New Password"
                                        name="password"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Please enter New Password",
                                            },
                                        ]}
                                    >
                                        <Input.Password size="large" />
                                    </Form.Item>
                                    <Form.Item
                                        label="Confirm Password"
                                        name="cpassword"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Please enter Confirm Password",
                                            },
                                        ]}
                                    >
                                        <Input.Password size="large" />
                                    </Form.Item>
                                    <Button loading={auth.isFetchingData} htmlType="submit" size="large" className="btn">
                                        Reset
                                    </Button>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    );
};

export default inject("auth")(observer(ResetPassword));
