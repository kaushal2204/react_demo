import { Button, Col, Form, Input, Row } from "antd";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import forgot_password from "../../../assets/images/forgot_password.png";
import "../../../assets/less/index.less";

const ForgotPassword = ({ auth }) => {
    const handleFinish = (values) => {
        auth.forgotPassword(values);
    };

    return (
        <>
            <div className="miq-theme content forgot-password">
                <div className="box">
                    <Row gutter={24} className="img">
                        <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
                            <div className="img">
                                <img alt="Img" src={forgot_password} />
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
                            <div className="form">
                                <h1>Forgot Password</h1>
                                <Form onFinish={handleFinish} layout="vertical">
                                    <Form.Item
                                        label="Email"
                                        name="email"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Please enter Email",
                                            },
                                        ]}
                                    >
                                        <Input size="large" />
                                    </Form.Item>
                                    <p>
                                        <Link to="/">Login ?</Link>
                                    </p>
                                    <Button loading={auth.isFetchingData} htmlType="submit" size="large" className="btn">
                                        Send
                                    </Button>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    );
};

export default inject("auth")(observer(ForgotPassword));
