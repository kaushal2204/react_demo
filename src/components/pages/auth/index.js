import { Button, Col, Form, Input, Row } from "antd";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import login from "../../../assets/images/login.svg";
import "../../../assets/less/index.less";

const Login = ({ auth }) => {
    const handleFinish = (values) => {
        auth.login(values);
    };
    return (
        <>
            <div className="miq-theme content login">
                <div className="box">
                    <Row gutter={24} className="img">
                        <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
                            <div className="img">
                                <img alt="Img" src={login} />
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
                            <div className="form">
                                <h1>Sign In</h1>
                                <Form onFinish={handleFinish} layout="vertical">
                                    <Form.Item
                                        label="Email"
                                        name="email"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Please enter Email",
                                            },
                                        ]}
                                    >
                                        <Input size="large" />
                                    </Form.Item>
                                    <Form.Item
                                        label="Password"
                                        name="password"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Please enter Password",
                                            },
                                        ]}
                                    >
                                        <Input.Password size="large" />
                                    </Form.Item>
                                    <p>
                                        <Link to="/forgot-password">Forgot Password ?</Link>
                                    </p>
                                    <Button loading={auth.isFetchingData} htmlType="submit" size="large" className="btn">
                                        Login
                                    </Button>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    );
};

export default inject("auth")(observer(Login));
