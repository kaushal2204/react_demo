import { Button } from "antd";
import { useNavigate } from "react-router-dom";
import img404 from "../../../assets/images/404.svg";

export default function Page404(props) {
    const navigate = useNavigate();
    return (
        <>
            <div className="miq-theme p-404">
                <div className="img">
                    <img alt="Img" src={img404} />
                </div>
                <p>We're Sorry, the page you requested could not found. Please go back to the Homepage</p>
                <Button size="large" className="btn" onClick={() => navigate("/")}>
                    Back to Home
                </Button>
            </div>
        </>
    );
}
